package main;
import EmailValidator.EmailValidator;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmailValidatorTest {
    private static EmailValidator emailValidator;

    @BeforeAll
    static void setup(){
        emailValidator = new EmailValidator();
    }

    @Test
    void EmailIsValidTest_ValidEmail_ReturnsTrue () {
        assertEquals(true, emailValidator.EmailIsValid("email@email.com"));
    }

    @Test
    void EmailIsValidTest_NoAtSymbol_ReturnsFalse () {
        assertEquals(false, emailValidator.EmailIsValid("emailemail.com"));
    }

    @Test
    void EmailIsValidTest_HasInvalidSymbol_ReturnsFalse () {
        assertEquals(false, emailValidator.EmailIsValid("email@email.com!"));
    }

    @Test
    void EmailIsValidTest_InvalidDomain_ReturnsFalse () {
        assertEquals(false, emailValidator.EmailIsValid("email@baddomain"));
    }

    @Test
    void EmailIsValidTest_InvalidTLD_ReturnsFalse () {
        assertEquals(false, emailValidator.EmailIsValid("abcd"));
    }
}

