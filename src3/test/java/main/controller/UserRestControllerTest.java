package main.controller;

import main.model.User;
import main.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@WebMvcTest(value = UserRestController.class)
public class UserRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService service;

    @Test
    void testShowUsers() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(new User(1, "TestName", "TestLastName", "861111111", "TestName@gmail.com", "Address", "Password1!"));
        users.add(new User(2, "TestName2", "TestLastName2", "862222222", "TestName2@gmail.com", "Address2", "Password2!"));

        when(service.findAll()).thenReturn(users);

        RequestBuilder rb = MockMvcRequestBuilders
                .get("/users")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(rb)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String expected = "[{\n" +
                "        \"id\": 1,\n" +
                "        \"firstName\": \"TestName\",\n" +
                "        \"lastName\": \"TestLastName\",\n" +
                "        \"phoneNumber\": \"861111111\",\n" +
                "        \"email\": \"TestName@gmail.com\",\n" +
                "        \"address\": \"Address\",\n" +
                "        \"password\": \"Password1!\"\n" +
                "    },\n" +
                "    {\n" +
                "        \"id\": 2,\n" +
                "        \"firstName\": \"TestName2\",\n" +
                "        \"lastName\": \"TestLastName2\",\n" +
                "        \"phoneNumber\": \"862222222\",\n" +
                "        \"email\": \"TestName2@gmail.com\",\n" +
                "        \"address\": \"Address2\",\n" +
                "        \"password\": \"Password2!\"\n" +
                "    }]";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    void testShowById() throws Exception {
        User user = new User(1, "TestName", "TestLastName", "861111111", "TestName@gmail.com", "Address", "Password1!");

        when(service.findById(Mockito.anyInt())).thenReturn(user);

        RequestBuilder rb = MockMvcRequestBuilders
                .get("/users/1")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(rb)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();

        String expected = "{\n" +
                "        \"id\": 1,\n" +
                "            \"firstName\": \"TestName\",\n" +
                "            \"lastName\": \"TestLastName\",\n" +
                "            \"phoneNumber\": \"861111111\",\n" +
                "            \"email\": \"TestName@gmail.com\",\n" +
                "            \"address\": \"Address\",\n" +
                "            \"password\": \"Password1!\"\n" +
                "    }";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }

    @Test
    void testDeleteUser() throws Exception {
        RequestBuilder rb = MockMvcRequestBuilders
                .delete("/users/1");

        MvcResult result = mockMvc.perform(rb).andReturn();

        MockHttpServletResponse response = result.getResponse();

        Assertions.assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

}
