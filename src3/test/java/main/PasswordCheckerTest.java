package main;

import PasswordChecker.PasswordChecker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class PasswordCheckerTest {
    private PasswordChecker passwordChecker;

    @BeforeAll
    void setup(){
        passwordChecker = new PasswordChecker();
    }

    @Test
    void PasswordIsValidTest_ValidPassword_ReturnsTrue () {
        assertEquals(true, passwordChecker.PasswordIsValid("Password!"));
    }

    @Test
    void PasswordIsValidTest_PasswordTooShort_ReturnsFalse () {
        assertEquals(false, passwordChecker.PasswordIsValid("Pas!"));
    }

    @Test
    void PasswordIsValidTest_NoUppercaseSymbol_ReturnsFalse () {
        assertEquals(false, passwordChecker.PasswordIsValid("password1"));
    }

    @Test
    void PasswordIsValidTest_NoSpecialSymbol_ReturnsFalse () {
        assertEquals(false, passwordChecker.PasswordIsValid("Password"));
    }
}
