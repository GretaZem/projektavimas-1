package main.jpa;

import main.model.User;
import main.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserCommandLineRunner implements CommandLineRunner {
    @Autowired
    private UserRepository repository;

    @Override
    public void run(String... args) throws Exception {
        repository.save(new User("TestName", "TestLastName", "861111111", "TestName@gmail.com", "Address", "Password1!"));
        repository.save(new User("TestName2", "TestLastName2", "862222222", "TestName2@gmail.com", "Address2", "Password2!"));
        repository.save(new User("TestName3", "TestLastName3", "863333333", "TestLastName3@gmail.com", "Address3", "Password3!"));
    }
}
