package main.controller;

import EmailValidator.EmailValidator;
import PasswordChecker.PasswordChecker;
import PhoneValidator.PhoneValidator;
import main.model.User;
import main.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class UserRestController {
    @Autowired
    UserService service;

    EmailValidator emailValidator = new EmailValidator();
    PasswordChecker passwordChecker = new PasswordChecker();
    PhoneValidator phoneValidator = new PhoneValidator();

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return service.findAll();
    }

    @GetMapping("/users/{userId}")
    public User getUserById(@PathVariable int userId) {
        return service.findById(userId);
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User user) throws Exception {
        if (user != null) {
            validateUserInfo(user);

            return service.addUser(user);
        }
        throw new Exception("No user object in request body.");
    }

    @PostMapping("/users/{userId}")
    public User updateUserById(@PathVariable int userId, @RequestBody User userInfo) throws Exception {
        if (service.doesUserExist(userId)) {
            userInfo.setId(userId);

            validateUserInfo(userInfo);
            service.updateUser(userInfo);

            return userInfo;
        }
        throw new Exception("User with specified id not found.");
    }

    @DeleteMapping("/users/{userId}")
    public void deleteUserById(@PathVariable int userId) {
        service.deleteUserById(userId);
    }

    private void validateUserInfo(User user) throws Exception {
        if (!emailValidator.EmailIsValid(user.getEmail())) {
            throw new Exception("Invalid email.");
        }
        if (!passwordChecker.PasswordIsValid(user.getPassword())) {
            throw new Exception("Invalid password.");
        }
        if (!phoneValidator.PhoneIsValid(user.getPhoneNumber())) {
            throw new Exception("Invalid phoneNumber");
        }
        if (Character.compare('+', user.getPhoneNumber().charAt(0)) != 0) {
            user.setPhoneNumber(phoneValidator.ChangePhoneToCountryCode(user.getPhoneNumber()));
        }
    }
}
