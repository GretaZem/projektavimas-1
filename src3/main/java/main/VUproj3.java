package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication // scan for components in current package and below
public class VUproj3 {

	public static void main(String[] args) {
		SpringApplication.run(VUproj3.class, args);
	}
}
