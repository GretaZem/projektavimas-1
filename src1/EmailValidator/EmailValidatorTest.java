package EmailValidator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EmailValidatorTest {
	EmailValidator emailValidator;
	
	@BeforeEach
	void setUp() throws Exception {
		emailValidator = new EmailValidator();
	}

	@Test
	void EmailIsValidTest_ValidEmail_ReturnsTrue () {
		assertEquals(true, emailValidator.EmailIsValid("email@email.com"));
	}

	@Test
	void EmailIsValidTest_NoAtSymbol_ReturnsFalse () {
		assertEquals(false, emailValidator.EmailIsValid("emailemail.com"));
	}

	@Test
	void EmailIsValidTest_HasInvalidSymbol_ReturnsFalse () {
		assertEquals(false, emailValidator.EmailIsValid("email@email.com!"));
	}
	
	@Test
	void EmailIsValidTest_InvalidDomain_ReturnsFalse () {
		assertEquals(false, emailValidator.EmailIsValid("email@baddomain"));
	}

	@Test
	void EmailIsValidTest_InvalidTLD_ReturnsFalse () {
		assertEquals(false, emailValidator.EmailIsValid("abcd"));
	}
}
