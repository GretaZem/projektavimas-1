
package com.company;

public class PasswordChecker {
	String[] specialCharacters = new String[] {".", "@", "?", "!", "#", "$", "%", "^", "&", "*", "(", ")", "-", "/", "~", "[", "]"} ;

    public boolean passwordLength(String password, int length) {
        if (password.length() >= length) {
            return true;
        }
        return false;
    }

    public boolean hasUpperCase(String password) {
        return !password.equals(password.toLowerCase());
    }

    public boolean hasSpecialCharacter(String password) {
        for (String specialChar : specialCharacters) {
        	if (password.contains(specialChar)) {
        		return true;
        	}
        }
        return false;
    }

}
