
package com.company;

public class PhoneValidator {
	String digits = "1234567890";
	
    public boolean notEmpty(String number) {
        return !number.isEmpty();
    }

    public boolean onlyNumbers(String number) {
        for (char character : number.toCharArray()) {
        	if (!digits.contains(String.valueOf(character))) {
        		return false;
        	}
        }
        return true;
    }

    public boolean numberLength(String number, int length) {
        if (number.length() >= length) {
            return true;
        }
        return false;
    }

    public boolean checkCountryCode(String number, String country) {
    	String code;
    	switch (country) {
    		case "Lithuania": {
    			code = "+370";
    			break;
    		}
    		case "USA": {
    			code = "+1";
    			break;
    		}
    		default: {
    			return false;
    		}
    	}
    	if (number.startsWith(code))
    		return true;
    	
    	return false;
    }

}
