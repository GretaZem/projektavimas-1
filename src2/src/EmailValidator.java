
package com.company;

public class EmailValidator {
		String[] badCharacters = new String[] {"?", "!", "#", "$", "%", "^", "&", "*", "(", ")", "-", "/", "~", "[", "]"} ;
		
        public boolean hasAtSign(String email) {
            return email.contains("@");
        }

        public boolean checkBadSymbols(String email) {
            for (String badSymbol : badCharacters) {
            	if (email.contains(badSymbol)) {
            		return false;
            	}
            }
            return true;
        }

        // validation criteria taken from
        // https://www.geeksforgeeks.org/how-to-validate-a-domain-name-using-regular-expression/
        // after consult with tester
        public boolean correctTLDCheck(String email) {
        	String tld = email.substring(email.lastIndexOf('.')+1);
        	if (tld.length() < 2 || tld.length() > 6)
        		return false;
            return true;
        }

        // validation criteria (partially) taken from
        // https://www.geeksforgeeks.org/how-to-validate-a-domain-name-using-regular-expression/
        // after consult with tester
        public boolean correctDomainCheck(String email) {
        	String domainName = email.substring(email.indexOf('@')+1, email.lastIndexOf('.'));
        	
        	//check length
        	if (domainName.length() < 1 || domainName.length() > 63)
        		return false;
        	
        	if (domainName.charAt(0) == '-')
        		return false;
        	
        	for (int i = 0; i < domainName.length(); i++) {
        		if (!Character.isLetterOrDigit(domainName.charAt(i)))
        			return false;
        	}
            
            return true;
        }

        public boolean notEmpty(String email) {
            return !email.isEmpty();
        }
}
