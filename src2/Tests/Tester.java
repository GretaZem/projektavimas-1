//test source https://github.com/Kazhiunea/ProgramuSustemuProjektavimas/commits/master
package com.company;

import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Tester {

        PasswordChecker pass = new PasswordChecker();
        PhoneValidator phone = new PhoneValidator();
        EmailValidator email = new EmailValidator();


        //Tests for the PasswordChecker class

        //Test: is the password length greater than 8
        //True
        @Test
        void passwordValidator_isLongerThanX() {
            assertTrue(pass.passwordLength("password",8));
        }

        //False
        @Test
        void passwordValidator_isNotLongerThanX() {
            //assertEquals("The password must be at least 8 characters", pass.hasUpperCase("pass"));
            assertEquals(false, pass.hasUpperCase("pass")); //Greta: changed test due to inconsistency
        }

        //Test: does the password have an uppercase letter
        //True
        @Test
        void passwordValidator_hasUppercase() {
            assertTrue(pass.hasUpperCase("Password"));
        }

        //False
        @Test
        void passwordValidator_hasNoUppercase() {
            //assertEquals("The password must contain an uppercase letter", pass.hasUpperCase("password"));
            assertEquals(false, pass.hasUpperCase("password")); //Greta: changed test due to inconsistency
         }

        //Test: does the password have special symbols
        //True
        @Test
        void passwordValidator_hasSpecialCharacters() {
            assertTrue(pass.hasSpecialCharacter("Password@"));
        }

        //False
        @Test
        void passwordValidator_hasNoSpecialSymbol() {
            //assertEquals("The password must contain a special Symbol (!, ?, @, ect.)", pass.hasUpperCase("Password"));
            assertEquals(false, pass.hasSpecialCharacter("Password"));//Greta: changed test due to inconsistency
        }

        //Tests for the PhoneValidator class

        //Test: is the inputted number not a null
        //True
        @Test
        void phoneValidator_isNotEmpty() {
            assertTrue(phone.notEmpty("+37061234567"));
        }

        //False
        @Test
        void phoneValidator_isEmpty() {
            //assertEquals("Please insert the phone number", phone.notEmpty(""));
            assertEquals(false, phone.notEmpty(""));//Greta: changed test due to inconsistency
        }

        //Test: Are there only integers in the number
        //True
        @Test
        void phoneValidator_hasOnlyNumbers() {
            assertTrue(phone.onlyNumbers("861234567"));
        }

        //False
        @Test
        void phoneValidator_hasCharacters() {
            //assertEquals("Unexpected characters in the number", phone.onlyNumbers("A61234567"));
            assertEquals(false, phone.onlyNumbers("A61234567"));//Greta: changed test due to inconsistency
        }

        //Test: The number has the correct amount of integers
        @Test
        void phoneValidator_isNotLongerThanX() {
        	//Greta: changed test for consistency, according to password length check
            assertTrue(phone.numberLength("+37061234567",12));
        }

        //Test: The country code is valid
        @Test
        void phoneValidator_validCountryCode() {
            assertTrue(phone.checkCountryCode("+37061234567","Lithuania"));
        }

        //Tests for the EmailValidator Class
        //Test: The email entered is not null
        @Test
        void emailValidator_isNotEmpty() {
            assertTrue(email.notEmpty("email@email.com"));
        }

        //Test: The string contains the "@" sign
        @Test
        void emailValidator_hasAtSign() {
            assertTrue(email.hasAtSign("email@email.com"));
        }

        //Test: The string contains no unexpected symbols
        @Test
        void emailValidator_hasNoBadSymbols() {
            assertTrue(email.checkBadSymbols("email@email.com"));
        }

        //Test: The string has the correct extention
        @Test
        void emailValidator_hasCorrectTLD() {
            assertTrue(email.correctTLDCheck("email@email.com"));
        }

        //Test: The strinng has the correct domain
        @Test
        void emailValidator_hasCorrectDomain() {
            assertTrue(email.correctDomainCheck("email@email.com"));
        }

}
